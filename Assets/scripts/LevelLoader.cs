﻿using UnityEngine;
using System.Collections.Generic;

// Contains static functions to load a level from a json data file in the Assets.
public class LevelLoader {

    public static void LoadLevelInto(string jsonFilePath, World world)
    {
		Random.InitState (0);
        string jsonText = LoadResourceTextfile(jsonFilePath);

        DataFromPyxeleditJsonFile data = LoadFromJsonString(jsonText);

        world.sizeX = data.tileswide;
        world.sizeY = data.tileshigh;
        Debug.Log("Loading world with sizeX=" + world.sizeX + ", sizeY=" + world.sizeY);

        world.playerCount = 4;
        world.players = new Player[world.playerCount];
        world.players[0] = new Player(0, new GridPos(6, 8));
        world.players[1] = new Player(1, new GridPos(10, 16));
        world.players[2] = new Player(2, new GridPos(9, 11));
        world.players[3] = new Player(3, new GridPos(11, 11));

        world.enemies = new List<Enemy>(); // zero enemies

        world.orderLists = new OrderList[world.playerCount];
        for (int i = 0; i < world.playerCount; i++) world.orderLists[i] = new OrderList();

		world.playerInventories = new Inventory[world.playerCount];
		for (int i = 0; i < world.playerCount; i++) world.playerInventories[i] = new Inventory();

        world.fields = new GridField[world.sizeX,world.sizeY];
        for(int x=0; x<world.sizeX; x++)
        {
            for (int y = 0; y < world.sizeY; y++)
            {
                world.fields[x, y] = new GridField(0, new GridPos(x,y)); // pass 1: initialize with tile index 0
            }
        }
        foreach (TileData tileData in data.tiles)
        {
            // pass 2: overwrite with tile index from json
            if (tileData.tile <= 0) continue; // found a -1 in the indexes - why? just skip it
            world.fields[tileData.x, tileData.y] = new GridField(tileData.tile, new GridPos(tileData.x, tileData.y));
        }

		world.fields [13, 9].SetItem(new PickupItem (PickupItem.ItemType.MagicOrb));
		world.fields [12, 4].SetItem(new PickupItem (PickupItem.ItemType.FireExtinguisher));
		world.fields [13, 8].SetItem(new PickupItem (PickupItem.ItemType.Gun));

		world.fields [6, 9].SetTrigger(new Trigger());
		world.fields [12, 15].SetTrigger(new Trigger());

    }

    private static string LoadResourceTextfile(string jsonFilePath)
    {
        string filePath = jsonFilePath.Replace(".json", "");
        TextAsset targetFile = Resources.Load<TextAsset>(filePath); // looks in Assets/Resourses/<given path>
        return targetFile.text;
    }


    private static DataFromPyxeleditJsonFile LoadFromJsonString(string jsonText)
    {
        // JsonUtility does not support nesting -> we get only simple top-level attributes with the first FromJson() call (no tile data yet)
        DataFromPyxeleditJsonFile data = JsonUtility.FromJson<DataFromPyxeleditJsonFile>(jsonText);

        // Hack: explicitly string-parse the json into parts and then use the JsonUtility on the (flat) parts individually

        List<string> tilesSections = ExtractTilesSections(jsonText);
        string tilesSection = tilesSections[0]; // pick the first one for now (we can have multiple with multiple layers)
        //Debug.Log("  extracted first tiles section: " + tilesSection);

        List<string> tileJsonFragments = ExtractTileJsonFragments(tilesSection);
        foreach (string tileJsonFragment in tileJsonFragments)
        {
            //Debug.Log("  parsing json: " + tileJsonFragment);
            TileData tileData = JsonUtility.FromJson<TileData>(tileJsonFragment);

            // ----- hack to add exits to level!!
            if (tileData.x == 6 && tileData.y == 14) tileData.tile = 0;
            if (tileData.x == 14 && tileData.y == 14) tileData.tile = 0;
            // ----- end hack

            data.tiles.Add(tileData);
            //Debug.Log("  read tile with x="+ tileData.x + ", y=" + tileData.y + ", tileIndex=" + tileData.tile );
        }

        return data;
    }


    private static List<string> ExtractTilesSections(string jsonText) // tiles section = list of tiles
    {
        int idx = 0;
        List<string> result = new List<string>();
        while (true)
        {
            int idxTiles = jsonText.IndexOf("\"tiles\": [", idx);
            if (idxTiles == -1) break;
            int idxClose = jsonText.IndexOf("]", idxTiles);
            if (idxClose == -1) break;
            string s = jsonText.Substring(idxTiles, idxClose - idxTiles);
            result.Add(s);
            idx = idxClose;
        }
        return result;
    }

    private static List<string> ExtractTileJsonFragments(string jsonText) // one fragment for each tile (in a tiles section)
    {
        int idx = 0;
        List<string> tileJsonFragments = new List<string>();
        while (true)
        {
            int idxOpen = jsonText.IndexOf("{", idx);
            if (idxOpen == -1) break;
            int idxClose = jsonText.IndexOf("}", idxOpen);
            if (idxClose == -1) break;
            string tileJsonFragment = jsonText.Substring(idxOpen, idxClose-idxOpen+1);
            tileJsonFragments.Add(tileJsonFragment);
            idx = idxClose;
        }
        return tileJsonFragments;
    }

    public class DataFromPyxeleditJsonFile {
        // JsonUtility requires that field names here are the same as attribute names in json file
        public int tileshigh;
        public int tileswide;

        public List<TileData> tiles = new List<TileData>();
    }

    public class TileData
    {
        public int x;
        public int y;
        public int tile;

        // loaded from json fragment, for example:
        // {
        //   "rot": 0,
        //    "y": 7,
        //    "flipX": false,
        //    "tile": 4,
        //    "index": 3,
        //    "x": 3
        // },
        //
        // or (different order):
        //
        // {
        //    "flipX": false,
        //    "y": 0,
        //    "rot": 0,
        //    "tile": 0,
        //    "index": 0,
        //    "x": 0
        //  },

    }


}

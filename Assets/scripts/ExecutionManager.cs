﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Deals with execution of orders.
// One turn consists of n steps. For each step, all orders are converted into Action s, which are then performed.
public class ExecutionManager
{
    public const int numberOfEnemySteps = 4;

    public static void DoStepPlayers()
    {
        // Get (and remove) first order in list for each player.
        OrderList.Order[] nextOrders = new OrderList.Order[World.cur.playerCount];
        for (int i = 0; i < World.cur.playerCount; i++)
        {
            if (World.cur.orderLists[i].list.Count > 0)
            {
                nextOrders[i] = World.cur.orderLists[i].list[0];
                World.cur.orderLists[i].list.RemoveAt(0);
            }
            else
            {
                nextOrders[i] = OrderList.Order.Null;
            }
        }

        // first goal is to fill the following actions
        Actions.Action[] actions = new Actions.Action[World.cur.playerCount];

        //
        for (int pass = 0; pass <= 10; pass++)
        {
            CreateActionsForAllPlayers(nextOrders, actions);
        }

        // Execute player actions (trigger animation, change data in model)
        for (int i = 0; i < World.cur.playerCount; i++)
        {
            Player player = World.cur.players[i];
            if (actions[i] != null && !(actions[i] is Actions.Nop))
            {
                player.DoStep(actions[i]);
            }
        }

    }

    // tries to populate entries in actions
    private static void CreateActionsForAllPlayers(OrderList.Order[] orders, Actions.Action[] actions)
    {
        for (int i = 0; i < World.cur.playerCount; i++)
        {
            if (actions[i] != null) continue; // done already for this player
            OrderList.Order playerOrder = orders[i];
            if (playerOrder == OrderList.Order.Null)
            {
                actions[i] = new Actions.Nop(); // order list of player was already empty -> do nothing
                continue;
            }
            Player player = World.cur.players[i];
            Actions.Action action = CreateActionForOnePlayer(orders, actions, player, playerOrder);
            if (action != null)
            {
                actions[i] = action;
            }
        }
    }

    // Tries to populate determine an action for the player (one pass). This method can be called multiple times for the same player.
    // Returns action: either null (try again later, maybe the field the player wants to enter is left by another player 
    // in another pass?) or not null (action determined now, nop=explicitly do nothing).
    private static Actions.Action CreateActionForOnePlayer(OrderList.Order[] orders, Actions.Action[] actions, Player player, OrderList.Order playerOrder)
    {
        if (OrderList.HasDirection(playerOrder))
        {
            GridDirection dir = OrderList.GetDirection(playerOrder);
            GridPos destination = player.gridPos.add(dir);
            GridField field = World.cur.GetField(destination);
            if (field.fire != null && World.cur.playerInventories[player.number].has(PickupItem.ItemType.FireExtinguisher))
            {//extinguish fire
                GameManager.Destroy(field.fire.unityObject);
                GridField.allFires.Remove(field.fire);
                field.fire = null;
                GameManager.instance.audioSource.PlayOneShot(GameManager.instance.fireExtinguish);

            }
            Enemy killedEnemy = null;
            foreach (Enemy enemy in World.cur.enemies)
            {
                if (GridPos.AreEqual(enemy.gridPos, destination) && World.cur.playerInventories[player.number].has(PickupItem.ItemType.Gun))
                {//kill monster
                    GameManager.instance.StartCoroutine(StaticUtils.AnimationFlickerAndKill(enemy.unityObject, false));
                    killedEnemy = enemy;
                }
            }
            if (killedEnemy != null) World.cur.enemies.Remove(killedEnemy);

            if (!World.IsOnGrid(destination)) return new Actions.Nop(); // cant leave grid
            if (!World.CanWalkOnField(destination)) return new Actions.Nop(); // cant walk onto that type of field/tile
            if (!World.CanEnterField(destination))
            {
                if(AnyPlayerWillLeaveField(destination, World.cur.players, actions) && ! AnyPlayerWillEnterField(destination, actions))
                {
                    Debug.Log("Player can enter field anyway because other player leaves that field"); // --> just proceed
                } else
                {
                    return null;
                }
            }

            if (field.trigger != null)
            {
                field.trigger.turn = GameManager.turnNumber;
                bool triggers = true;
                foreach (Trigger t in GridField.allTriggers)
                {
                    if (t.turn != GameManager.turnNumber)
                    {
                        triggers = false;
                        break;
                    }
                }
                if (triggers)
                {
                    GridPos teleportPos = new GridPos(3, 3);
                    Vector3 teleport = World.ToUnityCoords(teleportPos);
                    Debug.Log(player.unityObject.transform.position + ":" + teleport);
                    GameManager.instance.StartCoroutine(StaticUtils.AnimationMove(player.unityObject, player.unityObject.transform.position, teleport));
                    player.gridPos = teleportPos;
                    //player.unityObject.transform.position = teleport;
                    return new Actions.Nop();
                }
            }

            return new Actions.Move(destination);
        }
        else
        {
            // should be a non-movement action - todo
            return new Actions.Nop();
        }
    }

    // if any player is currently on pos and will leave it
    private static bool AnyPlayerWillLeaveField(GridPos pos, Player[] players, Actions.Action[] actions)
    {
        foreach(Player player in players) {
            if(GridPos.AreEqual(player.gridPos, pos) // player is on that field pos
                && actions[player.number]!=null  // player has action set already
                && actions[player.number] is Actions.Move) // action is a move
            {
                return true;
            }
        }
        return false;
    }

    private static bool AnyPlayerWillEnterField(GridPos pos, Actions.Action[] actions)
    {
        foreach (Actions.Action action in actions)
        {
            if (action != null // player has action set already
                && action is Actions.Move  // action is a move
                && GridPos.AreEqual(((Actions.Move) action).destination,pos) ) // action is a move to pos
            {
                return true;
            }
        }
        return false;
    }

    public static void DoStepEnemies()
    {
        // Execute enemy actions
        foreach (Enemy enemy in World.cur.enemies)
        {
            Actions.Action enemyAction = EnemyAI.CreateEnemyAction(enemy);
            if (!(enemyAction is Actions.Nop))
            {
                enemy.doStep(enemyAction);
            }
        }
    }

}

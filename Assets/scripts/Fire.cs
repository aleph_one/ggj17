﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : VisibleWorldObject {
	public static float SPREAD_PROB = 0.2f;
	public GridPos gridPos;

	public Fire () {
	}
	public override GameObject CreateUnityObject()
	{
		GameObject go = GameObject.Instantiate (Resources.Load<GameObject> ("Fire"));
		go.transform.position = World.ToUnityCoords(gridPos);
		Debug.Log("Created Fire at "+gridPos+" at unity coords " + go.transform.position);
		return go;
	}
}

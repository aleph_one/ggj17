﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Contains classes for different sctions (that result from Orders) and utility functions for actions.
public class Actions {

    public abstract class Action
    {
    }

    public class Move : Action
    {
        public GridPos destination;
        public bool isKill; // true if move onto alien player (which kills alien)
        public int killedPlayerNumber; // -1 if no kill
        public Move(GridPos destination)
        {
            this.destination = destination;
            this.isKill = false;
            this.killedPlayerNumber = -1;
        }
        public Move(GridPos destination, bool isKill, int killedPlayerNumber)
        {
            this.destination = destination;
            this.isKill = isKill;
            this.killedPlayerNumber = killedPlayerNumber;
        }
    }

    public class PickUpItem : Action
    {
        public PickUpItem()
        {
        }
    }

    public class Nop : Action
    {
        public Nop()
        {
			//Debug.Log("NOP");
        }
    }

}

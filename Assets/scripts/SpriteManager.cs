﻿using System;
using UnityEngine;

public static class SpriteManager {

    public const int tileIndex_emptyShip = 0;
    public const int tileIndex_grass = 1;

    private static Sprite[] tileSprites;

    public static Sprite GetTileSpriteByIndex(int index)
    {
        if(tileSprites==null)
        {
            tileSprites = Resources.LoadAll<Sprite>("spritesheets/tileset");
        }
        return tileSprites[index];
    }

    public static Sprite getPlayerSpriteByPlayerIndex(int number)
    {
        if (tileSprites == null)
        {
            tileSprites = Resources.LoadAll<Sprite>("spritesheets/tileset");
        }
        int index = 7 + number;
        return tileSprites[index];
    }

    public static bool canWalkOnTile(int tileIndex)
    {
        return tileIndex == 0 || tileIndex == 1;
    }
}

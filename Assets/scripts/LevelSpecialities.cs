﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Stuff specif for selected levels (stuff we cannot put into the level editor)
public class LevelSpecialities {

    // call this at the end of a turn before player give any orders
    public static void InitiateSpecialEvents(int turnNumber)
    {
        if (GameManager.instance.curLevel == GameManager.LEVEL1) InitiateSpecialEvents_level1(turnNumber);
    }

	private static void InitiateSpecialEvents_level1(int turnNumber) {
		Debug.Log ("special events" + turnNumber);
		//FIRE
		//existing fire spreading
		List<Fire> newFires = new List<Fire> ();
		foreach (Fire f in GridField.allFires) {
			GridField[] fields = GridDirection.GetAllWalkableDirections (f.gridPos);
			foreach (GridField nField in fields) {
				if (nField.fire == null && UnityEngine.Random.value < Fire.SPREAD_PROB) {
					Fire fire = new Fire ();
					fire.gridPos = nField.gridPos;
					nField.fire = fire;
					//nField.SetFire (fire);
					newFires.Add(fire);
					fire.RecreateUnityObject();
				}
			}
		}
		GridField.allFires.AddRange (newFires);

		//new fire outbreak
		if (UnityEngine.Random.Range(GridField.allFires.Count, 5) < 4) {
			GridPos pos = new GridPos(0, 0);
			do {
				pos.x = UnityEngine.Random.Range(3, World.cur.sizeX - 3);
				pos.y = UnityEngine.Random.Range(3, World.cur.sizeY - 3);

			} while (! World.CanWalkOnField(pos));

			Fire fire = new Fire ();
			World.cur.fields [pos.x, pos.y].SetFire(fire);
			fire.RecreateUnityObject ();
		}

		//ENEMIES
		if(turnNumber==3 || turnNumber == 6) {
            // spawn enemy 
            Enemy enemy = new Enemy(Enemy.EnemyType.Monster, new GridPos(6,6));
            // todo: check if field is blocked, chose another if so
            World.cur.enemies.Add(enemy);
            enemy.RecreateUnityObject();
            Debug.Log("Special event: spawned enemy");
        }
    }
}

using UnityEngine;
using System.Collections;
using System;

// Logical data model for the game.

public abstract class VisibleWorldObject
{
    public GameObject unityObject; // can possibly be null if, for some reason, the instance is not visible (?)

    // Creates the Unity (= the visual) representation of this model object. All subclasses need to implement this. 
    // If multiple game objects are to be returned then they can be organized in a hierarchy and only the top-level is returned.
    public abstract GameObject CreateUnityObject();

    public void RecreateUnityObject()
    {
        if (unityObject != null)
        {
            GameObject.Destroy(unityObject);
        }
        unityObject = CreateUnityObject();
    }
}

// All data for a player in the game.

// One field of the grid.

// A position on the grid (not to be confused with unity world coordinates, which are 3d floats)

using UnityEngine;
using System.Collections;
using System;

// Logical data model for the game.

// All data for a player in the game.
public class Player : VisibleWorldObject
{
    public int number; // same as index of this Player in players array
    public GridPos gridPos; // location of player on grid (or null if not on grid for some reason?)

    public Player(int number, GridPos gridPos)
    {
        this.number = number;
        this.gridPos = gridPos;
    }

    public override GameObject CreateUnityObject()
    {
        GameObject go = GameObject.Instantiate(GameManager.instance.playerPrefab);
        go.transform.position = World.ToUnityCoords(gridPos);
        go.GetComponent<SpriteRenderer>().sprite = SpriteManager.getPlayerSpriteByPlayerIndex(number);
        //go.GetComponent<SpriteRenderer> ().color = World.cur.playerColors [this.number];
        Debug.Log("Created GameObject for player at "+gridPos+" at unity coords " + go.transform.position);
        return go;
    }

    public Player DeepCloneWithoutUnityObjects()
    {
        return new Player(number, gridPos);
    }

    public void DoStep(Actions.Action action)
    {
		if (action is Actions.Move) {
			Actions.Move move = (Actions.Move)action;
			gridPos = move.destination;

			Vector3 target = World.ToUnityCoords (gridPos);
			GameManager.instance.StartCoroutine (StaticUtils.AnimationMove (unityObject, unityObject.transform.position, target));

			// pickup & inventory
			PickupItem item = World.cur.fields [gridPos.x, gridPos.y].item;
			if (item != null) {
				Inventory inventory = World.cur.playerInventories [number];
				if (inventory.Count() < Inventory.MAX_SIZE) {
					World.cur.fields [gridPos.x, gridPos.y].item = null;
					inventory.Add (item, number);
					//GameManager.Destroy (item.unityObject);
				}
			}

		} else if (action is Actions.Nop) {

		}
    }
}

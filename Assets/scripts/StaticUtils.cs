﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// for misc helper functions
public class StaticUtils {
    static public int foo;
    
    public static IEnumerator AnimationMove(GameObject go, Vector3 from, Vector3 destination)
    {
        float duration = GameManager.StepDuration / 2; // animation duration in secs
        for (float t = 0f; t < duration; t += Time.deltaTime)
        {
            go.transform.position = Vector3.Lerp(from, destination, t / duration);
            yield return 0;
        }
        go.transform.position = destination;
    }

	public static IEnumerator AnimationFlickerAndKill(GameObject go, bool lose)
    {
        float duration = GameManager.StepDuration; // animation duration in secs

        SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
        Vector3 origScale = sr.transform.localScale;
        sr.transform.localScale = origScale * 1.2f;

        for (float t = 0f; t < duration; t += Time.deltaTime)
        {
            if(Mathf.Sin(t*100)>0f) {
                go.SetActive(true);
            }
            else {
                go.SetActive(false);
            }
            yield return 0;
        }

        go.SetActive(true);
        sr.transform.localScale = origScale;
		GameManager.Destroy (go);
		if (lose) GameManager.instance.triggerPlayersLose();
    }

}

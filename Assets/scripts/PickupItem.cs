using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupItem : VisibleWorldObject {
	public ItemType type = ItemType.MagicOrb;
	public GridPos gridPos;

	public PickupItem(ItemType type) {
		this.type = type;
	}
	public override GameObject CreateUnityObject(){
		GameObject go = GameObject.Instantiate (Resources.Load<GameObject> (this.type.ToString()));
		go.transform.position = World.ToUnityCoords(gridPos);
		Debug.Log("Created PickupItem " + type + " at "+gridPos+" at unity coords " + go.transform.position);
		return go;
	}

	public enum ItemType {
		Gun, MagicOrb, FireExtinguisher
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioWaves : MonoBehaviour {

	private List<GameObject> waves = new List<GameObject>();
	private Color colorTransparent = new Color (1, 1, 1, 0);
	// Use this for initialization
	void Start () {
		for (int i = 0; i < 10; i++) {
			GameObject wave = GameObject.Instantiate (Resources.Load<GameObject> ("RadioWave"));
			wave.transform.parent = this.gameObject.transform;
			wave.transform.localPosition = Vector3.zero;
			wave.transform.localScale = Vector3.one * 0.2f * i;
			waves.Add (wave);
		}
		StartCoroutine (MakeWaves (waves));
	}

	public IEnumerator MakeWaves(List<GameObject> waves) {
		float duration = 2; // animation duration in secs
		for (float t = 0f; t < duration; t += Time.deltaTime) {
			for (int i = 0; i < 10; i++) {
				waves[i].transform.localScale = Vector3.Lerp(Vector3.one * 0.2f * i, Vector3.one * i, t / duration);
				waves[i].GetComponent<SpriteRenderer> ().color = Color.Lerp(Color.white, colorTransparent, t / duration);
			}
			yield return 0;
		}
		for (int i = 0; i < 10; i++) {
			waves[i].transform.localScale = Vector3.one * 0.2f * i;
		}
		StartCoroutine (MakeWaves (waves));
	}

	// Update is called once per frame
	private float lastWave = 0;
	void UpdateX () {
		float delta = Time.time - lastWave;
		if (delta > 0.2) {
			lastWave = Time.time;
			foreach (GameObject w2 in this.waves) {
				float xWobble = 1.25f +  Random.value / 10;
				float yWobble = 1.25f + Random.value / 10;
				Vector3 s = new Vector3 (w2.transform.localScale.x * xWobble, w2.transform.localScale.y * yWobble, w2.transform.localScale.z);
				//Debug.Log (w2.transform.localScale + "->" + s);
				w2.transform.localScale = s;
			}
			while (waves.Count > 12) {
				GameObject w3 = waves [0];
				GameManager.Destroy (w3);
				waves.RemoveAt (0);
			}
			//if (waves.Count < 6) {
				GameObject wave = GameObject.Instantiate (Resources.Load<GameObject> ("RadioWave"));
				wave.transform.parent = this.gameObject.transform;
				wave.transform.localPosition = Vector3.zero;
				waves.Add (wave);
			//}
		}
	}
}

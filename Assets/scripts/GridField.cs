using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

// Logical data model for the game.

// One field of the grid.
public class GridField : VisibleWorldObject
{
    public int tileIndex;
    public GridPos gridPos; // location of field on grid
	public PickupItem item;
	public Fire fire;
	public Trigger trigger;

    // todo for below fields (after jam): make below non-static and move somewhere else
	public static List<Fire> allFires = new List<Fire> ();    
	public static List<PickupItem> allItems = new List<PickupItem> ();
	public static List<Trigger> allTriggers = new List<Trigger> ();

    public GridField(int tileIndex, GridPos gridPos)
    {
        this.tileIndex = tileIndex;
        this.gridPos = gridPos;
    }

    public override GameObject CreateUnityObject()
    {
        Sprite sprite = SpriteManager.GetTileSpriteByIndex(tileIndex);
        GameObject go = GameObject.Instantiate(GameManager.instance.gridFieldPrefab);
        go.transform.position = World.ToUnityCoords(gridPos);
        go.GetComponent<SpriteRenderer>().sprite = sprite;
		go.transform.parent = GameManager.instance.gridParent.transform;
        return go;
    }

    public GridField DeepCloneWithoutUnityObjects()
    {
        return new GridField(tileIndex, gridPos);
    }

	public void SetTrigger(Trigger trigger) {
		this.trigger = trigger;
		this.trigger.gridPos = this.gridPos;
		allTriggers.Add (trigger);
	}

	public void SetFire(Fire fire) {
		this.fire = fire;
		this.fire.gridPos = this.gridPos;
		allFires.Add (fire);
	}
	public void SetItem(PickupItem item) {
		this.item = item;
		this.item.gridPos = this.gridPos;
		allItems.Add (item);
	}
}

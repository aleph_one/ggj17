﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderList {

    public const int MaxListSize = 12;

    public enum Order
    {
        North,
        South,
        West,
        East,
        Action,
        Null
    }

    public List<Order> list = new List<Order>();

    public static bool HasDirection(Order order)
    {
        return order == Order.North || order == Order.South || order == Order.West || order == Order.East;
    }

    public static GridDirection GetDirection(Order order)
    {
        if (order == Order.North) return GridDirection.DirNorth;
        if (order == Order.South) return GridDirection.DirSouth;
        if (order == Order.East) return GridDirection.DirEast;
        if (order == Order.West) return GridDirection.DirWest;
        return null;
    }

    public void Add(Order order) {
		list.Add (order);
	}

	public static void UpdateOrderLists(World world) {
		for (int player = 0; player < world.playerCount; player++) {
			for (int step = 0; step < OrderList.MaxListSize; step++) {
				GameObject slot = orderSlots [player, step];
				if (world.orderLists [player].list.Count <= step) {//reset to empty slot
					foreach (SpriteRenderer sr in slot.GetComponentsInChildren<SpriteRenderer> ()) {
						if (!sr.gameObject.name.StartsWith ("Player")) {
							sr.enabled = false;
						}
					}	
					break;
				}
				OrderList.Order order = world.orderLists [player].list [step];
				if (order == OrderList.Order.Action) {
					foreach (SpriteRenderer sr in slot.GetComponentsInChildren<SpriteRenderer> ()) {
						if (sr.gameObject.name.Equals ("Action")) {
							sr.enabled = true;
						} else if (!sr.gameObject.name.StartsWith ("Player")) {
							sr.enabled = false;
						}
					}	
				} else {
					SpriteRenderer sr = null;
					foreach (SpriteRenderer sr2 in slot.GetComponentsInChildren<SpriteRenderer> ()) {
						if (sr2.gameObject.name.Equals ("Arrow")) {
							sr = sr2;
							sr2.enabled = true;
						} else if (!sr2.gameObject.name.StartsWith ("Player")) {
							sr2.enabled = false;
						}
					}

					if (order == OrderList.Order.North) {
						sr.transform.eulerAngles = new Vector3 (0, 0, 90);
					} else if (order == OrderList.Order.East) {
						sr.transform.eulerAngles = new Vector3 (0, 0, 0);
					} else if (order == OrderList.Order.South) {
						sr.transform.eulerAngles = new Vector3 (0, 0, 270);
					} else if (order == OrderList.Order.West) {
						sr.transform.eulerAngles = new Vector3 (0, 0, 180);
					}
				}
			}
		}
	}

	private static GameObject[,] orderSlots = new GameObject[4, OrderList.MaxListSize];

	public static void CreateOrderLists(World world) {
		float radius = 7.5f;
		for (int player = 0; player < world.players.Length; player++) {
			float start = player * (2 * Mathf.PI /  world.players.Length);
			for (int step = 0; step < OrderList.MaxListSize; step++) {
				float offset = step * Mathf.PI / 24;
				float x = radius * Mathf.Sin (start + offset);
				float y =radius * Mathf.Cos (start + offset);
				GameObject slot = GameManager.Instantiate (GameManager.instance.orderSlot);
				slot.transform.parent = GameManager.instance.orderListParent.transform;
				orderSlots [player, step] = slot;
				slot.name = "Player" + player + ":" + step;
				slot.transform.position = new Vector2 (x, y);
				slot.GetComponent<SpriteRenderer> ().sortingOrder = 1;
				slot.GetComponent<SpriteRenderer> ().color = world.playerColors [player];

				foreach (SpriteRenderer sr in slot.GetComponentsInChildren<SpriteRenderer> ()) {
					sr.color = world.playerColors [player];
				}
			}
		}
	}

}

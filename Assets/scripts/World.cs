﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

// Logical data model for the game.
public class World {

    // Current world instance (= the game state that corresponds to what is currently displayed in the scene).
    public static World cur;

    // Size of grid (width, height)
    public int sizeX;
    public int sizeY;

    public int playerCount; // number of players at start (kills do not decrease this number)
    
    // Player instances, dimension must match playerCount. Players can be referenced by index they have in this array (0..n-1)
    public Player[] players;

    public List<Enemy> enemies;

	public Color[] playerColors = {Color.red, new Color(1, 0.2f, 0.6f), Color.blue, new Color(1, 0.6f, 0.2f)};
	public Inventory[] playerInventories;

    // one list per player, must not be null (no orders=empty list)
    public OrderList[] orderLists;

    // GridField[x][y] with dimensions sizeX, sizeY
    public GridField[,] fields;

    public World()
    {
    }

    public void RecreateAllUnitObjects()
    {
        foreach (Player p in players) p.RecreateUnityObject();
        foreach (GridField f in fields) f.RecreateUnityObject();
		foreach (PickupItem f in GridField.allItems) f.RecreateUnityObject();
		foreach (Trigger f in GridField.allTriggers) f.RecreateUnityObject();
    }

    public void RemoveAllUnitObjects()
    {
        foreach (Player p in players)     {  GameManager.Destroy(p.unityObject);  p.unityObject = null;    }
        foreach (GridField f in fields) { GameManager.Destroy(f.unityObject); f.unityObject = null; }
        foreach (PickupItem f in GridField.allItems) { GameManager.Destroy(f.unityObject); f.unityObject = null; }
        foreach (Trigger f in GridField.allTriggers) { GameManager.Destroy(f.unityObject); f.unityObject = null; }
		GridField.allTriggers.Clear ();
		GridField.allFires.Clear ();
		GridField.allItems.Clear ();
    }

    public World DeepCloneWithoutUnityObjects() // not used yet, we might use it for replaying the execution of a turn (reset to cloned state).
    {
        World w = new World();
        w.sizeX = sizeX;
        w.sizeY = sizeY;
        w.players = new Player[players.Length];
        for (int i = 0; i < players.Length; i++)
        {
            w.players[i] = players[i].DeepCloneWithoutUnityObjects();
        }
        w.fields = new GridField[sizeX, sizeY];
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                w.fields[x,y] = fields[x, y].DeepCloneWithoutUnityObjects();
            }
        }
        return w;
    }

    // Helper method for translating logical grid coords to unity coords (which we need to set on GameObjects)
    public static Vector3 ToUnityCoords(GridPos gridPos) 
    {
        int maxSize = System.Math.Max(cur.sizeX, cur.sizeY);
        float unitySizePerField = 20.0f / maxSize; // applies both to x and y dimension

        float gridPosXCentered = gridPos.x - (cur.sizeX-1)/2.0f;
        float gridPosYCentered = gridPos.y - (cur.sizeY-1)/2.0f;

        return new Vector3(
            gridPosXCentered * unitySizePerField,
            - gridPosYCentered * unitySizePerField,
            0
        );
    }

    public static bool IsOnGrid(GridPos gridPos)
    {
        return gridPos.x >= 0 && gridPos.y >= 0 && gridPos.x < World.cur.sizeX && gridPos.y <= World.cur.sizeY;
    }

    public static bool IsOnGrid(int gridX, int gridY)
    {
        return gridX >= 0 && gridY >= 0 && gridX < World.cur.sizeX && gridY <= World.cur.sizeY;
    }

    public GridField GetField(GridPos gridPos)
    {
        return fields[gridPos.x, gridPos.y];
    }

    public GridField GetField(int gridX, int gridY)
    {
        return fields[gridX, gridY];
    }

    // checks just if, in principle, player can walk on this field. Does not consider items, enemies, other players blocking field etc.
    public static bool CanWalkOnField(GridPos gridPos)
    {
        return CanWalkOnField(gridPos.x, gridPos.y);
    }

    public static bool CanWalkOnField(int gridX, int gridY)
    {
        if (!IsOnGrid(gridX, gridY)) return false; 
        int tileIndex = World.cur.GetField(gridX, gridY).tileIndex;
        return SpriteManager.canWalkOnTile(tileIndex);
    }

    // does ALL checks necessary if someone can currently enter this field
    public static bool CanEnterField(GridPos gridPos)
    {
        if (!CanWalkOnField(gridPos)) return false;
        foreach (Player player in World.cur.players)
            if (GridPos.AreEqual(player.gridPos, gridPos)) return false; // blocked by player
        foreach (Enemy enemy in World.cur.enemies)
            if (GridPos.AreEqual(enemy.gridPos, gridPos)) return false; // blocked by enemy
		if (World.cur.fields [gridPos.x, gridPos.y].fire != null) return false;//blocked by fire
        return true;
    }

    public static bool CanEnterFieldForEnemyAI(int gridX, int gridY)
    {
        if (!IsOnGrid(gridX, gridY)) return false;
        int tileIndex = World.cur.GetField(gridX, gridY).tileIndex;
        return SpriteManager.canWalkOnTile(tileIndex);
        // todo: exclude fire fields
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EnemyAI
{
    private const int maxDistance = 6; // a too high value might cause long calculation times

    public static Actions.Action CreateEnemyAction(Enemy enemy)
    {
        // Attempt 1: if player is on neighboring field then walk there
        foreach (GridDirection dir in GridDirection.ALL)
        {
            GridPos dest = enemy.gridPos.add(dir);
            foreach(Player p in World.cur.players) {
                if(GridPos.AreEqual(p.gridPos, dest))
                {
                    Debug.Log("Enemy does kill move");
                    return new Actions.Move(dest, true, p.number);
                }

            }
        }

        // calc walking distances from enemy for all fields nearby
        int[,] distFromEnemyGrid = new int[World.cur.sizeX, World.cur.sizeY];
        CalcWalkingDistancesGrid(enemy.gridPos, distFromEnemyGrid, maxDistance);
        LogGrid("from enemy grid", distFromEnemyGrid);

        // get player with smallest walking distance from this enemy
        Player nearestPlayer = null;
        int nearestWalkingDistance = 9999;
        foreach (Player player in World.cur.players)
        {
            int dist = distFromEnemyGrid[player.gridPos.x, player.gridPos.y];
            if (dist != -1 && dist < nearestWalkingDistance)
            {
                nearestPlayer = player;
                nearestWalkingDistance = dist;
            }
        }

        //
        if (nearestPlayer == null)
        {
            Actions.Action action = TryCreateRandomWalkerAction(enemy);
            return action;
        }

        // Calc walking distances from nearest player for all fields nearby
        Debug.Log("Nearest player is " + nearestPlayer.number + " with dist=" + nearestWalkingDistance);
        int[,] distFromNearestPlayerGrid = new int[World.cur.sizeX, World.cur.sizeY];
        CalcWalkingDistancesGrid(nearestPlayer.gridPos, distFromNearestPlayerGrid, maxDistance);
        LogGrid("from nearest player grid", distFromEnemyGrid);

        // pass 1: try to walk nearer to player if possible
        foreach (GridDirection dir in GridDirection.ALL)
        {
            GridPos dest = enemy.gridPos.add(dir);
            if (World.CanEnterField(dest) && distFromNearestPlayerGrid[dest.x, dest.y] < distFromNearestPlayerGrid[enemy.gridPos.x, enemy.gridPos.y]) {
                Debug.Log("Enemy moves nearer to player");
                return new Actions.Move(dest);
            }
        }
        // pass 2: try to walk such that distance to player stays the same
        foreach (GridDirection dir in GridDirection.ALL)
        {
            GridPos dest = enemy.gridPos.add(dir);
            if (World.CanEnterField(dest) && distFromNearestPlayerGrid[dest.x, dest.y] <= distFromNearestPlayerGrid[enemy.gridPos.x, enemy.gridPos.y])
            {
                Debug.Log("Enemy moves but keeps same distance to player");
                return new Actions.Move(dest);
            }
        }

        return new Actions.Nop(); // fallback: do nop
    }

    private static void LogGrid(string gridType, int[,] dist)
    {
        string s = gridType + ": ";
        for (int y = 0; y < World.cur.sizeY; y++)
        {
            s += "\nLine: ";
            for (int x = 0; x < World.cur.sizeX; x++)
            {
                int d = dist[x, y];
                string val = (d == -1 ? "-" : "" + d);
                s += " " + val;
            }
        }
        Debug.Log(s);
    }

    private static Actions.Action TryCreateRandomWalkerAction(Enemy enemy)
    {
        for(int i=0; i<10; i++) // 10 attempts
        {
            GridDirection dir = GridDirection.GetRandomDirection();
            GridPos dest = enemy.gridPos.add(dir);
            if (World.CanEnterField(dest)) return new Actions.Move(dest);
        }

        return new Actions.Nop(); // no good random move found -> nop
    }

    private static void CalcWalkingDistancesGrid(GridPos startingPos, int[,] distGrid, int maxSearchDistance)
    {
        for (int x = 0; x < World.cur.sizeX; x++)
            for (int y = 0; y < World.cur.sizeY; y++)
                distGrid[x, y] = -1; // -1 = not set
        FloodFillDistances(distGrid, startingPos.x, startingPos.y, maxSearchDistance, 0);
    }

    private static void FloodFillDistances(int[,] distGrid, int x, int y, int depthsLeft, int currentDistance)
    {
        if (depthsLeft == 0) return; // max depths reached -> prune
        if (!World.IsOnGrid(x, y)) return; // off grid -> prune
        if (!World.CanWalkOnField(x, y)) return; // cannot walk on field -> prune
        if (distGrid[x, y] != -1 && distGrid[x, y] <= currentDistance) return; // cell distance calculated already and at least as good -> prune
        distGrid[x, y] = currentDistance;
        FloodFillDistances(distGrid, x-1, y, depthsLeft - 1, currentDistance + 1);
        FloodFillDistances(distGrid, x, y-1, depthsLeft - 1, currentDistance + 1);
        FloodFillDistances(distGrid, x+1, y, depthsLeft - 1, currentDistance + 1);
        FloodFillDistances(distGrid, x, y+1, depthsLeft - 1, currentDistance + 1);
    }
}

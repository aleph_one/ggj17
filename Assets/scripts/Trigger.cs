﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : VisibleWorldObject {
	public GridPos gridPos;
	public int turn;

	public override GameObject CreateUnityObject(){
		GameObject go = GameObject.Instantiate (Resources.Load<GameObject> ("Trigger"));
		go.transform.position = World.ToUnityCoords(gridPos);
		Debug.Log("Created Trigger at "+gridPos+" at unity coords " + go.transform.position);
		return go;
	}

}

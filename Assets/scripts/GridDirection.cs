using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

// Logical data model for the game.

public class GridDirection
{
    public static GridDirection DirNorth = new GridDirection(0, -1);
    public static GridDirection DirSouth = new GridDirection(0, 1);
    public static GridDirection DirWest = new GridDirection(-1, 0);
    public static GridDirection DirEast = new GridDirection(1, 0);
	public static GridDirection[] ALL = {DirNorth, DirEast, DirSouth, DirWest};
    public int dx; // -1, 0, or 1
    public int dy; // -1, 0, or 1

    public GridDirection(int dx, int dy)
    {
        this.dx = dx;
        this.dy = dy;
    }

	public static GridField[] GetAllWalkableDirections(GridPos pos) {
		List<GridField> ds = new List<GridField> ();

		foreach (GridDirection dir in ALL) {
			GridPos nPos = new GridPos (pos.x + dir.dx, pos.y + dir.dy);
			if (World.CanWalkOnField (nPos)) {
				ds.Add (World.cur.fields [nPos.x, nPos.y]);
			}
		}
		GridField[] results = new GridField[ds.Count];
		ds.CopyTo (results);
		return results;
	}

    public static GridDirection GetRandomDirection()
    {
        int randomIndex = UnityEngine.Random.Range(0, 4);
        return GridDirection.ALL[randomIndex];
    }

    public override string ToString()
    {
        return "Direction[" + dx + "," + dy + "]";
    }
}

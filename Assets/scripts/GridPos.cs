using UnityEngine;
using System.Collections;
using System;

// Logical data model for the game.

// A position on the grid (not to be confused with unity world coordinates, which are 3d floats)
public class GridPos
{
    public int x; // zero or more
    public int y; // zero or more

    public GridPos(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public GridPos add(GridDirection d)
    {
        return new GridPos(x+d.dx,y+d.dy);
    }

    public override string ToString()
    {
        return "Grid["+x+","+y+"]";
    }

    // returns true if both params are not null and their coordinates are the same
    public static bool AreEqual(GridPos p1, GridPos p2)
    {
        if (p1 == null || p2 == null) return false;
        return p1.x == p2.x && p1.y == p2.y;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory {
	public static int MAX_SIZE = 6;
	private List<PickupItem> items = new List<PickupItem>();

	public int Count() {
		return this.items.Count;
	}
	public bool has(PickupItem.ItemType type) {
		foreach (PickupItem item in items) {
			if (item.type == type)
				return true;
		}
		return false;
	}

	public void Add(PickupItem item, int player) {
		this.items.Add (item);
		GridPos inv = new GridPos (0, 0);
		int offset = World.cur.playerInventories [player].Count();
		if (player == 0) {
			inv.x = 19;
			inv.y = 2 + offset;
		} else if (player == 1) {
			inv.x = 19;
			inv.y = 14 + offset;
		} else if (player == 2) {
			inv.x = 3;
			inv.y = 14 + offset;
		} else if (player == 3) {
			inv.x = 3;
			inv.y = 2 + offset;
		}

		Vector3 destination = World.ToUnityCoords (inv);
		GameManager.instance.StartCoroutine (StaticUtils.AnimationMove (item.unityObject, item.unityObject.transform.position, destination));

	}
}


﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null; // singleton instance

    public const float StepDuration = 0.5f; // how long the display of a step takes in seconds
    public const string LEVEL1 = "levels/level1.json";
    public const string LEVEL2 = "levels/level2.json";

    // Below GameObject are refs to prefabs manually set via the unity editor. These refs allow code in other classes to instantiate copies of these prefabs.
    public GameObject playerPrefab;
    public GameObject enemyPrefab;
    public GameObject gridFieldPrefab;
    public GameObject orderSlot;
    public GameObject levelFadeOutRectangle;
    public GameObject playerLoseText;
    public GameObject introPicture;
    public GameObject radio;

    public AudioClip mainMusic;
    public AudioClip fireExtinguish;
	public AudioClip ding;

    [HideInInspector]
	public AudioSource audioSource;

	public GameObject messages;
	[HideInInspector]
	public GameObject gridParent;
	[HideInInspector]
	public GameObject orderListParent;

    [HideInInspector]
    public string curLevel = LEVEL1; // current level.

    public enum GamePhase
    {
        InitializingGame, // happens only once
        InitializingLevel, // happens multiple times = each time a level is started/restarted
        Commands, // players are entering commands to their queues
        ExecutionForPlayers, // order queues of players are executed
        ExecutionForEnemies,
        SpecialEvents, // like spawning enemies

        // special phases
        PlayersLose
    }

    public static GamePhase gamePhase = GamePhase.InitializingGame;

    private static float gamePhaseStart;
    private static float gameStepLastStartTime; // used for execution phase only

    public static int turnNumber; // starts with 0, incremented right before command phase
    private static int stepNumber; // starts with 0, incremented right before step

    public static void SwitchToGamePhase(GamePhase newGamePhase)
    {
        Debug.Log("Switching phase: " + gamePhase + " ==> " + newGamePhase);
        gamePhase = newGamePhase;
		gamePhaseStart = Time.time;
		instance.audioSource.PlayOneShot (instance.ding);
        //this.fireExtinguish.
        // can switch music here depending on new phase etc
        if (gamePhase == GamePhase.InitializingLevel)
        {
            turnNumber = 0;
            stepNumber = 0;
        }
        if (gamePhase == GamePhase.Commands) {
			//instance.messages.SetActive (true);
			instance.messages.GetComponent<Text> ().text = "Aliens:\nEnter Commands!";
            //instance.messages.transform.parent.gameObject.GetComponent<Image>().enabled = true;
            instance.orderListParent.SetActive(true);
            instance.StartCoroutine (PhaseOut(instance.messages));
            turnNumber++;
            Debug.Log("Starting turn " + turnNumber);
        }

        if (gamePhase == GamePhase.ExecutionForPlayers)
        {
            gameStepLastStartTime = 0; // = time to do a step right now!
            stepNumber = 0;
            //instance.messages.SetActive (true);
            instance.messages.GetComponent<Text> ().text = "Execution Players";
            instance.StartCoroutine (PhaseOut(instance.messages));
        }

        if (gamePhase == GamePhase.ExecutionForEnemies)
        {
            gameStepLastStartTime = 0; // = time to do a step right now!
            stepNumber = 0;
            //instance.messages.SetActive (true);
            instance.messages.GetComponent<Text>().text = "Execution Enemies";
            instance.orderListParent.SetActive(false);
            instance.StartCoroutine(PhaseOut(instance.messages));
        }

        if (gamePhase == GamePhase.PlayersLose)
        {
            instance.StartCoroutine(AnimationPlayersLose(instance.levelFadeOutRectangle, instance.playerLoseText));
        }

    }
    public static IEnumerator PhaseOut(GameObject go) {
		yield return new WaitForSeconds(1);
		//go.SetActive (false);
		go.GetComponent<Text> ().text = "";
	}

    void Awake()
    {
        // singleton stuff
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    // Use this for initialization
    void Start () {
		this.audioSource = GetComponent<AudioSource> ();
        this.audioSource.PlayOneShot(mainMusic);

        //this.messages = GameObject.Find ("Messages");
        this.gridParent = new GameObject("GridParent");
        this.orderListParent = new GameObject("OrderListParent");

        // dummy load of world to make CreateOrderLists happy
        World world = new World();
        LevelLoader.LoadLevelInto(curLevel, world);

        OrderList.CreateOrderLists(world);

        instance.StartCoroutine(AnimationPlayIntro());
    }

    private void InitializeLevel()
    {
        // clean up old world
        if(World.cur!=null)
        {
            World.cur.RemoveAllUnitObjects();
            World.cur = null;
        }

        World world = new World();
        LevelLoader.LoadLevelInto(curLevel, world);
        World.cur = world;

        world.RecreateAllUnitObjects();
        SwitchToGamePhase(GamePhase.Commands);
    }


    private int turnDuration = 15;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();

        if (Input.GetKeyDown(KeyCode.F1)) SwitchToGamePhase(GamePhase.PlayersLose);

        if (gamePhase == GamePhase.Commands)
        {
            if (Time.time - gamePhaseStart < 1)
            {
                return;// 1 second pause before start of turn; corresponds to phase change text shown for one second
            }

            int timeLeft = turnDuration - (int)(Time.time - gamePhaseStart);
            this.messages.GetComponent<Text>().text = "" + timeLeft;
            foreach (OrderList.Order n in System.Enum.GetValues(typeof(OrderList.Order)))
            {
                if (n == OrderList.Order.Null)
                    continue;
                for (int i = 0; i < World.cur.playerCount; i++)
                {
					bool b = Input.GetButtonUp("Player" + i +  n.ToString());
                    if (b)
                    {
                        Debug.Log(n.ToString() + i + ": " + b);
                        //add action to OrderList
                        if (World.cur.orderLists[i].list.Count < OrderList.MaxListSize)
                        {
                            World.cur.orderLists[i].list.Add(n);
                            OrderList.UpdateOrderLists(World.cur);
                        }
                    }
                }
            }

            if ((Time.time - gamePhaseStart) > turnDuration || Input.GetKeyDown(KeyCode.F12))
            {
                SwitchToGamePhase(GamePhase.ExecutionForPlayers);
            }
        }
        else if (gamePhase == GamePhase.ExecutionForPlayers)
        {
            if (Time.time - gamePhaseStart < 1.0)
            {
                return;// 1 second pause before start of turn; corresponds to phase change text shown for one second
            }

            if ((Time.time - gameStepLastStartTime) > StepDuration)
            {
                gameStepLastStartTime = Time.time;
                stepNumber++;
                ExecutionManager.DoStepPlayers();
                OrderList.UpdateOrderLists(World.cur);
                if (stepNumber >= OrderList.MaxListSize)
                    SwitchToGamePhase(GamePhase.ExecutionForEnemies);
            }
        }
        else if (gamePhase == GamePhase.ExecutionForEnemies)
        {
            if (Time.time - gamePhaseStart < 1.0)
            {
                return;// 1 second pause before start of turn; corresponds to phase change text shown for one second
            }

            if ((Time.time - gameStepLastStartTime) > StepDuration)
            {
                Debug.Log("DoStep Enemies");
                gameStepLastStartTime = Time.time;
                stepNumber++;
                ExecutionManager.DoStepEnemies();
                OrderList.UpdateOrderLists(World.cur);
                if (stepNumber >= ExecutionManager.numberOfEnemySteps)
                    SwitchToGamePhase(GamePhase.SpecialEvents);
            }
        }
        else if (gamePhase == GamePhase.SpecialEvents)
        {
            LevelSpecialities.InitiateSpecialEvents(turnNumber);
            SwitchToGamePhase(GamePhase.Commands);
        }
        else if (gamePhase == GamePhase.PlayersLose)
        {
            // nothing. when animation is ready, it will change phase
        }
    }

    // called from game logic
    public void triggerPlayersLose()
    {
        SwitchToGamePhase(GamePhase.PlayersLose);
    }


    private static IEnumerator AnimationPlayersLose(GameObject levelFadeOutRectangle, GameObject playerLoseText)
    {
        instance.messages.GetComponent<Text>().text = ""; // clear any messages that might currently be displayed

        SpriteRenderer sr = levelFadeOutRectangle.GetComponent<SpriteRenderer>();
        SpriteRenderer sr2 = playerLoseText.GetComponent<SpriteRenderer>();

        {
            // fade out level by increasing alpha value of big black rectangle
            float duration = 1f;
            for (float t = 0f; t < duration; t += Time.deltaTime)
            {

                Color tmp = sr.color;
                tmp.a = t / duration;
                sr.color = tmp;

                Color tmp2 = sr2.color;
                tmp2.a = t / duration;
                sr2.color = tmp2;

                yield return 0;
            }
        }

        {
            float duration = 3f;
            for (float t = 0f; t < duration; t += Time.deltaTime)
            {
                Color tmp2 = sr2.color;

                float x = t / duration * 3 * (2 * Mathf.PI) + Mathf.PI/2;

                tmp2.a = Mathf.Sin(x) *0.3f + 0.7f;
                sr2.color = tmp2;

                yield return 0;
            }
        }

        {
            Color tmp = sr.color;
            tmp.a = 0f; // fully transparent = not visible
            sr.color = tmp;

            Color tmp2 = sr2.color;
            tmp2.a = 0f; // fully transparent = not visible
            sr2.color = tmp2;
        }
        SwitchToGamePhase(GamePhase.InitializingLevel);
        instance.InitializeLevel();
    }

    private IEnumerator AnimationPlayIntro()
    {
        instance.orderListParent.SetActive(false);
        instance.radio.SetActive(false);

        for (int i=0; i<17;i++) 
        {
            if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Escape)) break;

            Sprite sprite = Resources.Load<Sprite>("story1/story_1-"+i);
            SpriteRenderer sr = introPicture.GetComponent<SpriteRenderer>();
            sr.sprite = sprite;
            yield return new WaitForSeconds(3f);
        }

        introPicture.SetActive(false);
        instance.orderListParent.SetActive(true);
        instance.radio.SetActive(true);

        SwitchToGamePhase(GamePhase.InitializingLevel);
        InitializeLevel();
    }


}

﻿using UnityEngine;
using System.Collections;
using System;

// All data for an enemy in the game.
public class Enemy : VisibleWorldObject
{

    public enum EnemyType
    {
        Monster
    }

    public EnemyType type; 
    public GridPos gridPos; // location of enemy on grid

    public Enemy(EnemyType type, GridPos gridPos)
    {
        this.type = type;
        this.gridPos = gridPos;
    }

    public override GameObject CreateUnityObject()
    {
        GameObject go = GameObject.Instantiate(GameManager.instance.enemyPrefab);
        // todo: set sprite depending on type
        go.transform.position = World.ToUnityCoords(gridPos);
        return go;
    }

    public Enemy DeepCloneWithoutUnityObjects()
    {
        return new Enemy(type, gridPos);
    }

    public void doStep(Actions.Action action)
    {
        Debug.Log("Doing Action for enemy: " + action);
        if (action is Actions.Move)
        {
            Actions.Move move = (Actions.Move)action;
            gridPos = move.destination;
            Vector3 target = World.ToUnityCoords(gridPos);
            GameManager.instance.StartCoroutine(StaticUtils.AnimationMove(unityObject, unityObject.transform.position, target));

            if (move.isKill)
            {
                Player killedPlayer = World.cur.players[move.killedPlayerNumber];
                GameObject go = killedPlayer.unityObject;
                GameManager.instance.StartCoroutine(StaticUtils.AnimationFlickerAndKill(go, true));
            }
        }
    }

}

